import requests, os
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from app_auth.utils import get_header, fetch

response = {}


def get_auth(request):
    try:
        jwt_token = request.COOKIES.get("jwt_")
        user_id = request.COOKIES.get("user_id")
    except Exception as e:
        return "", ""
    return jwt_token, user_id


@get_header
def index_new(request, header):
    url = 'api/profile/1/'
    api_response = fetch(url, header)
    if api_response.status_code == 401:
        return HttpResponseRedirect(reverse('auth:login'))
    api_response = api_response.json()
    api_response['user_mahasiswa']['headline'] = 'Computer Science Student at Universitas Indonesia'
    api_response['user_mahasiswa']['location'] = 'Jakarta, Depok, Cilegon'
    response['user'] = api_response
    return render(request, 'mahasiswa/index.html', response)


def vacancy(request):
    url = os.getenv('BE_URL', '') + 'api/lowongan'
    jwt_token, _ = get_auth(request)
    if jwt_token is None:
        return HttpResponseRedirect(reverse('auth:login'))
    header = {'Authorization': 'JWT ' + jwt_token}
    api_response = requests.get(url, headers=header)
    if api_response.status_code == 401:
        return HttpResponseRedirect(reverse('auth:login'))
        # return HttpResponse("Error 403: Perboden nih. Mohon login dahulu.")
    api_response = api_response.json()
    response['vacancies'] = api_response
    return render(request, 'mahasiswa/vacancy.html', response)


def vacancy_detail(request, vacancy_id):
    url = os.getenv('BE_URL', '') + 'api/lowongan/' + vacancy_id + '/'
    jwt_token, _ = get_auth(request)
    if jwt_token is None:
        return HttpResponseRedirect(reverse('auth:login'))
    header = {'Authorization': 'JWT ' + jwt_token}
    api_response = requests.get(url, headers=header)
    print(api_response)
    if api_response.status_code == 401:
        return HttpResponseRedirect(reverse('auth:login'))
        # return HttpResponse("Error 403: Perboden nih. Mohon login dahulu.")
    api_response = api_response.json()
    response['vacancy'] = api_response
    return render(request, 'mahasiswa/vacancy_detail.html', response)
