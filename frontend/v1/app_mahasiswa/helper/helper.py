import requests

API_riwayat_list_URL = "https://private-e52a5-ppw2017.apiary-mock.com/riwayat"


class RiwayatHelper:
	class __RiwayatHelper:
		def get_riwayat_list(self):
			response = requests.get(API_riwayat_list_URL)
			riwayat_list = response.json()
			return riwayat_list

	instance = None

	def __init__(self):
		if not RiwayatHelper.instance:
		   RiwayatHelper.instance = RiwayatHelper.__RiwayatHelper()
