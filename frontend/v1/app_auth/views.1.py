from django.shortcuts import render, redirect, reverse
from django.http import (HttpResponseRedirect,
                         HttpResponseForbidden,
                         HttpResponseBadRequest)
from django.urls import reverse
from django.conf import settings
from django.http import HttpRequest, JsonResponse, HttpResponseRedirect
from django.contrib import messages
import requests, json
from app_company.models import Company, Job
from app_mahasiswa.models import User
from app_auth import (csui_helper, utils, forms)
from app_mahasiswa import utils as mhs_utils

response = {}

# Render login page
def login_page(request):
    if 'session-login' in request.session:
        response['nama'] = request.session['nama']
        response['status'] = "student"
    elif 'session-id' in request.session:
        response['nama'] = request.session['nama']
        response['status'] = "company"
    elif 'dummy-here' in request.session:
        response['nama'] = request.session['nama']
        response['status'] = "company"
    else:
        response['status'] = "none"
    html = 'login/login_page.html'
    return render(request, html, response)

def dummy_auth(request):
    is_exist = Company.objects.filter(name="Lorem Ipsum")
    if is_exist.count() == 0:
        company = Company.objects.create(
            id_linkedin="loremipsum",
            name="Lorem Ipsum",
            company_type="Lorem Ipsum Type",
            web="www.google.com",
            description="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            specialty="Lorem Ipsum Specialties",
            picture_url="https://picsum.photos/200",
        )
    is_dummy_job_exist = Job.objects.filter(title="Lorem Ipsum Job")
    if is_dummy_job_exist.count() == 0:
        company = Company.objects.get(name="Lorem Ipsum")
        job = Job.objects.create(
            company=company,
            title="Lorem Ipsum Job",
            description="This lorem ipsum job offer you Rp 50.000.000/month as long you love me",
        )
    request.session['dummy-here'] = "true"
    request.session['nama'] = "Lorem Ipsum"
    return redirect(reverse('company:profile'))

# 4 fungsi di bawah merupakan Company Login use linkedin account
CLIENT_ID = "866hc1so0n9js9"
CLIENT_SECRET = "IFSny4QeaB0XYT8D"

# company uth
def request_auth(request: HttpRequest):
    REDIRECT_URI = settings.BASE_URL + "auth/company/token/"
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

def request_token(request: HttpRequest):
    REDIRECT_URI = settings.BASE_URL + "auth/company/token/"
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"

    if request.method == "GET":
        code = request.GET.get('code','')
        state = request.GET.get('state','')

        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        request_header = {
            'content_type':"application/json"
        }

        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))

        request.session['session-id'] = response_data['access_token']
        new_company_id = get_company_id(request)
        request.session['company_id'] = new_company_id
        company_data = get_company_data(request)

        is_exist = Company.objects.filter(id_linkedin=new_company_id)
        if is_exist.count() == 0:
            specialties = ", ".join(company_data.get('specialties',{}).get('values',''))
            company = Company.objects.create(
            	id_linkedin=new_company_id,
                name=company_data.get('name', ''),
                company_type=company_data.get('companyType', {}).get('name', ''),
                web=company_data.get('websiteUrl', ''),
                description=company_data.get('description', ''),
                specialty=specialties,
                picture_url=company_data.get('squareLogoUrl', ''),
            )
        else:
            company = Company.objects.get(id_linkedin=new_company_id)
        request.session['nama'] = company_data.get('name', '')
        return redirect(reverse('company:profile'))
    else:
        return redirect('/')

#Mengambil id perusahaan dimana user terdaftar sebagai admin perusahaan tersebut di linkedin
def get_company_id(request):
    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/companies?oauth2_access_token="+token+"&format=json&is-company-admin=true"
    response = requests.request("GET", url=url)
    company = json.loads(response.content.decode('utf8'))
    size = len(company['values'])
    company_id = str(company['values'][size-1]['id'])
    return company_id

def get_company_data(request):
    token = request.session['session-id']
    company_id = request.session['company_id']
    url = "https://api.linkedin.com/v1/companies/"+company_id+":(id,name,ticker,description,company-type,website-url,specialties,square-logo-url)?oauth2_access_token="+token+"&format=json"
    company_data = requests.request("GET", url=url)
    company_data = json.loads(company_data.content.decode('utf8'))
    return company_data

    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

def request_auth_user(request: HttpRequest):
    REDIRECT_URI = settings.BASE_URL + "auth/user/linkedin/token/"
    uri = ("https://www.linkedin.com/oauth/v2/authorization?" +
           "response_type=code&" +
           "client_id=" + CLIENT_ID + "&" +
           "redirect_uri=" + REDIRECT_URI + "&" +
           "state=987654321&" +
           "scope=r_basicprofile r_emailaddress rw_company_admin w_share")
    return redirect(uri)

# updates user picture_url and id_linkedin
def request_token_user(request):
    REDIRECT_URI = settings.BASE_URL + "auth/user/linkedin/token/"
    request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"

    if request.method == "GET":
        code = request.GET.get('code','')
        state = request.GET.get('state','')

        request_data = {
            "grant_type":"authorization_code",
            "code":code,
            "redirect_uri":REDIRECT_URI,
            "client_id":CLIENT_ID,
            "client_secret":CLIENT_SECRET
        }

        request_header = {
            'content_type':"application/json"
        }

        response = requests.request("POST", request_token_uri, data=request_data,
                         headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))

        token = response_data['access_token']
        url = "https://api.linkedin.com/v1/people/~:(id,picture-url,siteStandardProfileRequest)?oauth2_access_token="+token+"&format=json"
        user_data = requests.request("GET", url=url)
        user_data = json.loads(user_data.content.decode('utf8'))

        mahasiswa = User.objects.get(npm=request.session['npm'])
        mahasiswa.picture_url = user_data.get('pictureUrl','')
        mahasiswa.id_linkedin = user_data.get('id','')
        mahasiswa.link_linkedin = user_data.get('siteStandardProfileRequest','').get('url','')
        mahasiswa.save()

        return redirect(reverse('mahasiswa:index'))
    else:
        return redirect('/')


    token = request.session['session-id']
    url = "https://api.linkedin.com/v1/people/~:(id,picture-url)?oauth2_access_token="+token+"&format=json"
    user_data = requests.request("GET", url=url)
    user_data = json.loads(company_data.content.decode('utf8'))
    return user_data

# mahasiswa login menggunakan SSO
def auth_login(request):
    form = forms.LoginForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():

        __USERNAME__ = request.POST['username']
        __PASSWORD__ = request.POST['password']

        try:
            access_token = csui_helper.get_access_token(__USERNAME__, __PASSWORD__)
        except csui_helper.SSOException as e:
            print(e)
            access_token = None

        if access_token:
            verified_user = csui_helper.verify_user(access_token=access_token)

            if __USERNAME__ == verified_user['username']:
                npm = verified_user['identity_number']
                role = verified_user['role']

                user_data = csui_helper.get_user_data(access_token=access_token, id=npm)
                user = mhs_utils.get_or_create_user(npm=npm,
                                                    name=user_data['nama'].title(),
                                                    role=role,
                                                    angkatan=user_data['program'][0]['angkatan']
                                                    )

                # set user session
                request.session['session-login'] = mhs_utils.serialize_user(user=user)
                request.session['npm'] = npm
                request.session['access_token'] = access_token
                request.session['nama'] = user_data['nama'].title()

            else:
                return HttpResponseForbidden(reason='Error: Unexpected behavior')

        else:
            messages.error(request, 'Login: Unidentified username or password')
            # return redirect(reverse('render:login'))

    else:
        return HttpResponseBadRequest(reason='Error: Unexpected method')

    # TODO FINISH WITH REVERSE URL
    return redirect(reverse('mahasiswa:index'))

# Logout untuk mahasiswa maupun company
def auth_logout(request):
    if request.method == 'GET':
        request.session.flush()
        print("flush")
    else:
        return HttpResponseBadRequest(reason='Error: Unexpected method')

    #TODO FINISH WITH REVERSE URL
    html = 'login/login_page.html'
    return HttpResponseRedirect('/auth/')