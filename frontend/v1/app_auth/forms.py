from django import forms

class LoginForm(forms.Form):
    error_messages = {
        'required': 'This is a required field',
    }

    username_attrs = {
        'type': 'text',
        'class': 'form-input',
        'id': 'username-input',
        'name': 'username-input',
        'cols': 50,
        'placeholder': 'SSO Username',
    }

    password_attrs = {
        'type': 'password',
        'class': 'form-input',
        'id': 'password-input',
        'name': 'password-input',
        'cols': 50,
        'placeholder': 'SSO Password',
    }

    username = forms.CharField(label='Username SSO', required=True, widget=forms.TextInput(attrs=username_attrs))
    password = forms.CharField(label='Password SSO', required=True, widget=forms.TextInput(attrs=password_attrs))