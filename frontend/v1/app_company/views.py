from django.shortcuts import render, redirect, reverse
from .models import Company, Job
from .forms import JobForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
def index(request):
    response['test'] = 'kons'
    return render(request, 'base.html', response)

def show_profile(request):
    if 'session-id' in request.session:
        return show_profile_by_id(request, request.session['company_id'])
    elif 'dummy-here' in request.session:
        company = Company.objects.get(name="Lorem Ipsum")
        return show_profile_by_id(request, company.id_linkedin)
    else:
        #TODO: Kasih error
        return redirect('/')

def show_profile_by_id(request, key="loremipsum"):
    company = Company.objects.get(id_linkedin=key)
    response['company'] = company
    response['status'] = "company"

    job_list = Job.objects.all().filter(company=company)
    paginator = Paginator(job_list, 3) #TODO: Enak cm 3 doang?

    page = request.GET.get('page')
    try:
        job = paginator.page(page)
    except:
        job = paginator.page(1)

    response['job'] = job
    return render(request, 'compProfile.html', response)


def add_job(request):
    if 'session-id' not in request.session or 'company_id' not in request.session:
        return redirect('/')
    if request.method == 'POST':
        form = JobForm(request.POST)
        response['form'] = form
        if form.is_valid():
            company = Company.objects.filter(id_linkedin=request.session["company_id"]).first()
            Job.objects.create(title=form.cleaned_data['title'],description=form.cleaned_data['description'], company=company)
            return redirect(reverse('company:profile'))
        else:
            job = Job.objects.all()
            response['job'] = job
            response['form'] = form
            return render(request, 'compProfile.html', response)

def delete_job(request, id):
    if 'session-id' not in request.session:
        return redirect('/')
    to_delete = Job.objects.get(id=id)
    to_delete.delete()
    return redirect(reverse('company:profile'))
