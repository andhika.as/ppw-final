/**
 *
 * AdminVerifPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import RegistrantList from 'app_admin/components/RegistrantList/Loadable';
import RegisteredUserDetail from 'app_admin/components/RegisteredUserDetail/Loadable';

import autobind from 'react-autobind';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAdminPage from './selectors';
import reducer from './reducer';
import saga from './saga';

import {
  getRegisteredUserStart,
  verifyUserStart,
} from './actions';

export class AdminVerifPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      chosenRegistrant: null,
    };
  }

  componentDidMount() {
    this.props.getRegisteredUser();
  }

  handleVerif() {
    this.props.verifyUser(this.state.chosenRegistrant);
  }
  handleRegistrant(registrant) {
    this.setState({
      chosenRegistrant: registrant,
    });
  }
  render() {
    const registrantData = this.props.adminpage.payload || [];
    console.log('aaa', this.props.adminpage);
    return (
      <div>
        <Helmet>
          <title>AdminVerifPage</title>
          <meta name="description" content="Description of AdminVerifPage" />
        </Helmet>
        <div className="container">
          <div className="row">
            <div className="col-8 border-right border-left">
              <RegistrantList registrantData={registrantData} handleRegistrant={this.handleRegistrant} />
            </div>
            <div className="col-4">
              <RegisteredUserDetail chosenRegistrant={this.state.chosenRegistrant} handleVerif={this.handleVerif} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AdminVerifPage.propTypes = {
  getRegisteredUser: PropTypes.func.isRequired,
  adminpage: PropTypes.object,
  verifyUser: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  adminpage: makeSelectAdminPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getRegisteredUser: () => dispatch(getRegisteredUserStart()),
    verifyUser: (payload) => dispatch(verifyUserStart(payload)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminPage', reducer });
const withSaga = injectSaga({ key: 'adminPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AdminVerifPage);
