/**
 *
 * AdminLogin
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import autobind from 'react-autobind';

import AdminLoginForm from 'app_admin/components/AdminLoginForm';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAdminLogin from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  loginStart,
} from './actions';

export class AdminLogin extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    autobind(this);
  }

  submit(username, password) {
    const payload = {
      username,
      password,
    };
    this.props.postLogin(payload);
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>AdminLogin</title>
          <meta name="description" content="Description of AdminLogin" />
        </Helmet>
        <AdminLoginForm submit={this.submit} />
      </div>
    );
  }
}

AdminLogin.propTypes = {
  postLogin: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  adminlogin: makeSelectAdminLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    postLogin: (payload) => dispatch(loginStart(payload)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminLogin', reducer });
const withSaga = injectSaga({ key: 'adminLogin', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AdminLogin);
