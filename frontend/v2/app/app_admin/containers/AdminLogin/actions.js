/*
 *
 * AdminLogin actions
 *
 */

import Cookies from 'universal-cookie';

import {
  DEFAULT_ACTION,
  POST_LOGIN_START,
  POST_LOGIN_SUCCESS,
  POST_LOGIN_FAIL,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function loginStart(payload) {
  return {
    type: POST_LOGIN_START,
    payload,
  };
}

export function loginSuccess(payload) {
  const { jwt } = payload.data;
  const cookies = new Cookies();
  cookies.set('jwt_', jwt, { path: '/' });

  return {
    type: POST_LOGIN_SUCCESS,
    payload,
  };
}

export function loginError(error) {
  return {
    type: POST_LOGIN_FAIL,
    error,
  };
}
