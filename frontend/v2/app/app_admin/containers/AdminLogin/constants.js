/*
 *
 * AdminLogin constants
 *
 */

export const DEFAULT_ACTION = 'app/AdminLogin/DEFAULT_ACTION';
export const POST_LOGIN_START = 'app/AdminLogin/POST_LOGIN_START';
export const POST_LOGIN_SUCCESS = 'app/AdminLogin/POST_LOGIN_SUCCESS';
export const POST_LOGIN_FAIL = 'app/AdminLogin/POST_LOGIN_FAIL';
