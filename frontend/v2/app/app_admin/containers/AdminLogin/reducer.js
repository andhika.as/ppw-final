/*
 *
 * AdminLogin reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  POST_LOGIN_SUCCESS,
  POST_LOGIN_START,
  POST_LOGIN_FAIL,
} from './constants';

const initialState = fromJS({
  loading: true,
  payload: null,
});

function adminLoginReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case POST_LOGIN_START:
      return state.merge({
        loading: true,
        payload: null,
      });
    case POST_LOGIN_SUCCESS:
      return state.merge({
        loading: false,
        payload: action.payload,
      });
    case POST_LOGIN_FAIL:
      return state.merge({
        loading: false,
        payload: action.error,
      });
    default:
      return state;
  }
}

export default adminLoginReducer;
