/**
 *
 * Asynchronously loads the component for RegisteredUserDetail
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
