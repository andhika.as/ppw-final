/*
 *
 * Homepage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  REQUEST_LOGIN_CS_AUTH,
  REQUEST_LOGIN_CS_AUTH_FAILED,
  REQUEST_LOGIN_CS_AUTH_SUCCESS,
  REQUEST_SIGN_UP_SULING,
  REQUEST_SIGN_UP_SULING_FAILED,
  REQUEST_SIGN_UP_SULING_SUCCESS,
} from './constants';

const initialState = fromJS({
  message: '',
  loading: false,
  jwt: '',
  user: {},
});

function homepageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case REQUEST_LOGIN_CS_AUTH:
      return state.merge({
        loading: true,
      });
    case REQUEST_LOGIN_CS_AUTH_FAILED: {
      return state.merge({
        message: action.payload,
        loading: false,
      });
    }
    case REQUEST_LOGIN_CS_AUTH_SUCCESS: {
      const {
        message,
        jwt,
        user,
      } = action.payload.payload;
      return state.merge({
        message,
        jwt,
        user,
        loading: false,
      });
    }
    case REQUEST_SIGN_UP_SULING: {
      return state.merge({
        loading: true,
      });
    }
    case REQUEST_SIGN_UP_SULING_SUCCESS: {
      return state.merge({
        loading: false,
        message: action.payload.payload.message,
      });
    }
    case REQUEST_SIGN_UP_SULING_FAILED: {
      return state.merge({
        loading: false,
        message: action.payload.payload.message,
      });
    }
    default:
      return state;
  }
}

export default homepageReducer;
