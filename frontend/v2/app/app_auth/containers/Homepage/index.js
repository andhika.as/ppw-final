/**
 *
 * Homepage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { SlEnvelopeOpen, SlLock, SlUser } from 'components/Icon/sl';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { postLoginCSAuth } from 'containers/App/actions';
import makeSelectHomepage from './selectors';
import reducer from './reducer';
import saga from './saga';
import logoUI from './img/logo-ui.svg';
import logoLinkedin from './img/linked-in.svg';
import logoDark from './img/logo_dark.svg';
import backgroundImg from './img/thumbnail.png';
import { postSignUpSuling } from './actions';

const MainWrapper = styled.div`
  .btn.btn-default.btn-flat {
    position: relative;
    padding: 10px;
    border: 0;
    font-family: "PT Sans", sans-serif;
    font-weight: 700;
  }
  .btn.btn-default.btn-flat .btn-icon {
    position: absolute;
    top: 0;
    left: 7px;
    height: 100%;
    display: flex;
    justify-content: center;
  }
  .btn.btn-default.btn-flat.btn-ui {
    background-color: #f2d600;
    color: white;
  }
  .btn.btn-default.btn-flat.btn-ui:hover {
    background-color: #d9bf00;
  }
  .btn.btn-default.btn-flat.btn-ui:active {
    background-color: #bfa900;
  }
  .btn.btn-default.btn-flat.btn-linkedin {
    background-color: #0077b5;
    color: white;
  }
  .btn.btn-default.btn-flat.btn-linkedin:hover {
    background-color: #00669c;
  }
  .btn.btn-default.btn-flat.btn-linkedin:active {
    background-color: #005582;
  }
  .btn.btn-default.btn-flat.btn-facebook {
    background-color: #3b5998;
    color: white;
  }
  .btn.btn-default.btn-flat.btn-facebook:hover {
    background-color: #344e86;
  }
  .btn.btn-default.btn-flat.btn-facebook:active {
    background-color: #2d4373;
  }
  .btn.btn-default.btn-flat.btn-google {
    background-color: #4da1ff;
    color: white;
  }
  .btn.btn-default.btn-flat.btn-google:hover {
    background-color: #3494ff;
  }
  .btn.btn-default.btn-flat.btn-google:active {
    background-color: #1a86ff;
  }

  .thumbnail__container {
    background: url(${props => props.url_img}) center center;
    background-size: cover;
    color: white;
    min-height: 80vh;
  }

  .box-rounded {
    overflow: hidden;
  }

  .hero__container {
    font-family: "Open Sans", sans-serif;
    background-color: rgba(213, 213, 255, 0.51);
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
  }

  .form__container {
    margin-right: -5em;
    z-index: 99;
  }

  .form__outer {
    z-index: 99;
  }

  /*# sourceMappingURL=style.css.map */
`;

const FormLogin = props => (
  <div>
    <form
      action=""
      onSubmit={props.handleSubmit}
      className="row align-items-stretch form__container my-4"
    >
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlUser color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            id="username"
            name="username"
            placeholder="username"
            title="keyword"
            type="text"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlLock color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            id="password"
            name="password"
            placeholder="password"
            title="keyword"
            type="password"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 text-right">
        <button
          type="submit"
          id="btn-submit"
          className="btn btn-primary btn-flat"
        >
          Login
        </button>
      </div>
    </form>
  </div>
);

const SignUpForm = props => (
  <div>
    <form
      action=""
      onSubmit={props.handleSubmit}
      className="row align-items-stretch form__container my-4"
    >
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlUser color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            name="username"
            placeholder="username"
            title="keyword"
            type="text"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlLock color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            name="password"
            placeholder="password"
            title="keyword"
            type="password"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlLock color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            name="repassword"
            placeholder="re-type password"
            title="keyword"
            type="password"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 mb-4">
        <div className="input-group input h-100">
          <div className="input-group-prepend input__prepend-icon">
            <span className="input-group-text">
              <SlEnvelopeOpen color="gray" size={16} />
            </span>
          </div>
          <input
            onChange={props.handleInputChange}
            name="email"
            placeholder="email"
            title="keyword"
            type="text"
            className="form-control box-shadow"
          />
        </div>
      </div>
      <div className="col-12 text-right">
        <button type="submit" className="btn btn-primary btn-flat">
          Register
        </button>
      </div>
    </form>
  </div>
);

export class Homepage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      username: null,
      password: null,
      email: null
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const { value, name } = event.target;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { username, password, email } = this.state;

    let requestUrl;
    const showLogin = this.props.match.params.behaviour === "login";
    if (showLogin) {
      requestUrl =
        this.props.match.params.vendor === "sso"
          ? `${process.env.BE_URL}auth/cs-auth/login`
          : `${process.env.BE_URL}auth/login`;
    }

    const { onPostLoginCSAuth, onPostSignUpSuling } = this.props;
    if (showLogin) {
      onPostLoginCSAuth(username, password, requestUrl);
    } else {
      onPostSignUpSuling(username, email, password);
    }
  }

  render() {
    const showLogin = this.props.match.params.behaviour === "login";
    const showSSOButtonLogin = this.props.match.params.vendor === "sso";
    return (
      <div>
        <Helmet>
          <title>Homepage</title>
          <meta name="description" content="Description of Homepage" />
        </Helmet>

        <MainWrapper url_img={backgroundImg}>
          <div className="container-fluid">
            <div className="container bg-white box-shadow box-rounded mt-5">
              <div className="row">
                <div className="col-5 form__outer">
                  <div className="container">
                    <div className="row mt-5 page__header">
                      <div className="col ">
                        <h1 className="page__title">
                          <strong>{showLogin ? `Login ${this.props.match.params.vendor}` : "Register"}</strong>
                        </h1>
                      </div>
                    </div>
                    <div className="row my-2">
                      <div className="col small">
                        <em>Akses semua fitur akan terbuka untukmu.</em>
                      </div>
                    </div>
                    {!showLogin && (
                      <SignUpForm
                        handleInputChange={this.handleInputChange}
                        handleSubmit={this.handleSubmit}
                      />
                    )}
                    {showLogin && (
                      <FormLogin
                        handleInputChange={this.handleInputChange}
                        handleSubmit={this.handleSubmit}
                      />
                    )}
                    {!showSSOButtonLogin &&
                      showLogin && (
                        <div className="row">
                          <div className="col">
                            <a
                              href="login/sso"
                              className="btn btn-default btn-flat btn-ui btn-block"
                            >
                              <span className="btn-icon">
                                <object
                                  type="image/svg+xml"
                                  data={logoUI}
                                  className="logo-image"
                                >
                                  UI Logo
                                </object>
                              </span>
                              Login via SSO
                            </a>
                            <a
                              href={`${process.env.BE_URL}auth/linkedin/login`}
                              className="btn btn-default btn-flat btn-linkedin btn-block"
                            >
                              <span className="btn-icon">
                                <object
                                  type="image/svg+xml"
                                  data={logoLinkedin}
                                  className="logo-image"
                                >
                                  Linkedin Logo
                                </object>
                              </span>
                              Login via Linkedin
                            </a>
                          </div>
                        </div>
                      )}
                    <div className="row my-4">
                      <div className="col small text-center text-muted">
                        <p>
                          {showLogin
                            ? "Belum punya akun?"
                            : "Sudah punya akun?"}
                          <br />
                          <a href={showLogin ? "signup" : "login"}>
                            {showLogin
                              ? "Buat akunmu sekarang!"
                              : "Login disini!"}
                          </a>
                          <br />
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-7 thumbnail__container d-flex align-items-center">
                  <div className="container">
                    <div className="row justify-content-center">
                      <div className="col-auto hero__container p-5">
                        <h2 className="mb-3">Welcome to</h2>
                        <img src={logoDark} alt="logo-app" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </MainWrapper>
      </div>
    );
  }
}

Homepage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  payload: PropTypes.shape({
    user: PropTypes.shape({
      user_mahasiswa: PropTypes.object
    })
  }),
  onPostLoginCSAuth: PropTypes.func,
  onPostSignUpSuling: PropTypes.func,
  match: PropTypes.object
};

FormLogin.propTypes = {
  handleSubmit: PropTypes.func,
  handleInputChange: PropTypes.func
};
SignUpForm.propTypes = FormLogin.propTypes;

const mapStateToProps = createStructuredSelector({
  homepage: makeSelectHomepage()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onPostLoginCSAuth: (username, password, requestUrl) =>
      dispatch(postLoginCSAuth(username, password, requestUrl)),
    onPostSignUpSuling: (username, email, password) =>
      dispatch(postSignUpSuling(username, email, password))
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: "app_auth", reducer });
const withSaga = injectSaga({ key: "app_auth", saga });

export default compose(withReducer, withSaga, withConnect)(Homepage);
