import { createSelector } from 'reselect';

/**
 * Direct selector to the homepage state domain
 */
const selectHomepageDomain = (state) => state.get('app_auth');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Homepage
 */

const makeSelectHomepage = () => createSelector(
  selectHomepageDomain,
  (substate) => substate.toJS()
);

export default makeSelectHomepage;
export {
  selectHomepageDomain,
};
