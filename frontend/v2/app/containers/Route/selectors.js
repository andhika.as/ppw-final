import { createSelector } from 'reselect';

/**
 * Direct selector to the route state domain
 */
const selectRouteDomain = (state) => state.get('customroute');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Route
 */

const makeSelectRoute = () => createSelector(
  selectRouteDomain,
  (substate) => substate.toJS()
);

export default makeSelectRoute;
export {
  selectRouteDomain,
};
