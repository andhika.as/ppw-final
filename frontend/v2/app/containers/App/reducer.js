/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from "immutable";

import {
  SET_LOADING,
  LOAD_REPOS_SUCCESS,
  LOAD_REPOS,
  LOAD_REPOS_ERROR,
  REQUEST_LOGIN_CS_AUTH,
  REQUEST_LOGIN_CS_AUTH_FAILED,
  REQUEST_LOGIN_CS_AUTH_SUCCESS,
  VERIFY_AUTH_REQUESTED,
  VERIFY_AUTH_FAILED,
  VERIFY_AUTH_SUCCESS
} from "./constants";

// The initial state of the App
const initialState = fromJS({
  loading: true,
  error: false,
  currentUser: false,
  isAuthenticated: false,
  userData: {
    repositories: false
  },
  user: {},
  auth: {}
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return state.set("loading", action.payload);
    case LOAD_REPOS:
      return state
        .set("loading", true)
        .set("error", false)
        .setIn(["userData", "repositories"], false);
    case LOAD_REPOS_SUCCESS:
      return state
        .setIn(["userData", "repositories"], action.repos)
        .set("loading", false)
        .set("currentUser", action.username);
    case LOAD_REPOS_ERROR:
      return state.set("error", action.error).set("loading", false);
    case REQUEST_LOGIN_CS_AUTH:
      return state.merge({
        loading: true
      });
    case REQUEST_LOGIN_CS_AUTH_FAILED: {
      return state.merge({
        message: action.payload,
        loading: false
      });
    }
    case REQUEST_LOGIN_CS_AUTH_SUCCESS: {
      const { message, jwt, user, user_id } = action.payload.payload;
      return state.merge({
        user,
        auth: {
          message,
          jwt,
          user_id
        },
        loading: false
      });
    }
    case VERIFY_AUTH_REQUESTED:
      return state.merge({ isAuthenticated: false, loading: true });
    case VERIFY_AUTH_SUCCESS:
      const { user, jwt, user_id, admin, message } = action.payload;
      return state.merge({
        isAuthenticated: true,
        loading: false,
        user,
        auth: {
          admin,
          message,
          jwt,
          user_id
        }
      });
    case VERIFY_AUTH_FAILED:
      return state.merge({ isAuthenticated: false, loading: false });
    default:
      return state;
  }
}

export default appReducer;
