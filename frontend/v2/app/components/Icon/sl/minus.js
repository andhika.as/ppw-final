
import React from 'react';
import Icon from 'react-icon-base';

const SlMinus = props => (
    <Icon viewBox="0 0 1024 1024" {...props}>
        <g><path transform="scale(1, -1) translate(0, -960)" d="M512 960c283 0 512 -229 512 -512s-229 -512 -512 -512s-512 229 -512 512s229 512 512 512zM512 -1c247 0 448 202 448 449s-201 448 -448 448s-448 -201 -448 -448s201 -449 448 -449zM477 416h-189c-18 0 -32 14 -32 32s14 32 32 32h190h258c18 0 32 -14 32 -32 s-14 -32 -32 -32h-259z"/></g>
    </Icon>
);

export default SlMinus;
