
import React from 'react';
import Icon from 'react-icon-base';

const SlDoc = props => (
    <Icon viewBox="0 0 1024 1024" {...props}>
        <g><path transform="scale(1, -1) translate(0, -960)" d="M560 960h-352c-35.3438 0 -64 -28.6562 -64 -64v-896c0 -35.3438 28.6562 -64 64 -64h608c35.3438 0 64 28.6562 64 64v639.984zM816 613.472v-5.47168h-288v288h5.50391zM208 0v896h256v-352h352v-544h-608z"/></g>
    </Icon>
);

export default SlDoc;
