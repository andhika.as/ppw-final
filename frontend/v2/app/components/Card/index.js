/**
*
* Card
*
*/

import React from 'react';
// import styled from 'styled-components';
import {
  CardLayout,
} from './styled';


function Card(props) {
  return (
    <div>
      <CardLayout background={props.backgroundColor} padding={props.padding} margin={props.margin}>
        {React.Children.toArray(props.children)}
      </CardLayout>
    </div>
  );
}

Card.propTypes = {
  children: React.PropTypes.node,
  backgroundColor: React.PropTypes.string,
  padding: React.PropTypes.string,
  margin: React.PropTypes.string,
};

export default Card;
