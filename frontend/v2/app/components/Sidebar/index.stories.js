import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import Sidebar from './index';

storiesOf('Sidebar', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic Sidebar component.`)(() => (
    <Sidebar>

    </Sidebar>
  ))
);
