/**
*
* Sidebar
*
*/

import React from 'react';
import styled from 'styled-components';
import autoBind from 'react-autobind';

const Wrapper = styled.div`
`;

class Sidebar extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    autoBind(this);
  }

  render() {
    return (
      <Wrapper className="sidebar__container container">
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-6 col-md-4 col-lg-12 p-lg-0 mt-3">
              <div className="row">
                <div className="col-12">
                  <div className="nav-link mt-2 item__item--highlighted">
                    {'Verifikasi User'}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
    );
  }
}

Sidebar.propTypes = {

};

export default Sidebar;
