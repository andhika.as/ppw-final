import React from "react";

import { storiesOf } from "@storybook/react";
import { text, withKnobs } from "@storybook/addon-knobs";
import { withInfo } from "@storybook/addon-info";

import Navbar from "./index";

storiesOf("Navbar", module)
  .addDecorator(withKnobs)
  .add(
    "basic",
    withInfo(``)(() => <Navbar homeLocation={text("logo location", "#")} />)
  );
