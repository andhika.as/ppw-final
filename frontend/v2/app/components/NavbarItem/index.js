/**
 *
 * NavbarItem
 *
 */

import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

const StyledLi = styled.li`
  padding-left: 1.5em;
  padding-right: 1.5em;

  .nav-link.active {
      position: relative;

      &:before {
        content: "";
        background-color: ${props => props.theme.primaryColor};
        width: 88%;
        height: 10%;
        position: absolute;
        bottom: 0;
      }
    }
`;

class NavbarItem extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const { children, location } = this.props;

    return (
      <StyledLi className="nav-item">
        <NavLink exact className="nav-link d-flex align-items-center" activeClassName="active" to={location}>
          {children}
        </NavLink>
      </StyledLi>
    );
  }
}

NavbarItem.propTypes = {
  profileImg: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  location: PropTypes.string,
};

NavbarItem.defaultProps = {
  location: '/',
};

export default NavbarItem;
