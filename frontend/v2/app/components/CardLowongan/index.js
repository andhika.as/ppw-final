/**
*
* CardLowongan
*
*/

import React from 'react';
import Image from 'components/Img';
import PropTypes from 'prop-types';
import CompanyPlaceholderImage from 'images/company_placeholder.png';
// import styled from 'styled-components';


function CardLowongan(props) {
  return (
    <div className="row vacancy__container">
      <div className="col-auto vacancy__company-logo">
        <Image
          circled
          alt="company-logo"
          src={props.vacancy.perusahaan.company_logo ? props.vacancy.perusahaan.company_logo : CompanyPlaceholderImage}
        />
      </div>
      <div className="col vacancy__body pl-lg-0">
        <div className="vacancy__title">
          {props.vacancy.position}
        </div>
        <div className="vacancy__company">
          {props.vacancy.perusahaan.name}
        </div>
        <div className="vacancy__location">
          {props.vacancy.location}
        </div>
      </div>
    </div>
  );
}

CardLowongan.propTypes = {
  vacancy: PropTypes.object,
};

export default CardLowongan;
