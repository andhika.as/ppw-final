/*
 *
 * Homepage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GET_PROFILE_SULING_REQUESTED,
  GET_PROFILE_SULING_SUCCESS,
  GET_PROFILE_SULING_ERROR,
  EDIT_PROFILE_SULING_REQUESTED,
  EDIT_PROFILE_SULING_SUCCESS,
  EDIT_PROFILE_SULING_ERROR,
  TOGGLE_EDIT,
} from './constants';

const initialState = fromJS({
  payload: {},
  loading: 'Memuat informasi...',
  error: '',
  editing: false,
  loadingEditProfile: '',
});

function homepageReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_PROFILE_SULING_REQUESTED:
      return state.merge({
        loadingEditProfile: 'Submitting changes...',
      });
    case EDIT_PROFILE_SULING_SUCCESS:
      return state.merge({
        loadingEditProfile: '',
      });
    case EDIT_PROFILE_SULING_ERROR:
      return state.merge({
        loadingEditProfile: '',
        error: `Gagal mengubah profile. ${action.payload}`,
      });
    case GET_PROFILE_SULING_REQUESTED:
      return state.merge(initialState.toJS());
    case GET_PROFILE_SULING_SUCCESS:
      return state.merge(initialState.toJS(), {
        loading: '',
        payload: action.payload,
      });
    case GET_PROFILE_SULING_ERROR:
      return state.merge(initialState.toJS(), {
        loading: '',
        error: `Gagal memuat informasi. ${action.payload}`,
      });
    case DEFAULT_ACTION:
      return state;
    case TOGGLE_EDIT:
      return state.merge({
        editing: !state.get('editing'),
      });
    default:
      return state;
  }
}

export default homepageReducer;
