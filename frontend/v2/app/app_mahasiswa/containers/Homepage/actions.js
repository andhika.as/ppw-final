/*
 *
 * Homepage actions
 *
 */

import {
  DEFAULT_ACTION,
  GET_PROFILE_SULING_REQUESTED,
  GET_PROFILE_SULING_SUCCESS,
  GET_PROFILE_SULING_ERROR,
  EDIT_PROFILE_SULING_REQUESTED,
  EDIT_PROFILE_SULING_SUCCESS,
  EDIT_PROFILE_SULING_ERROR,
  TOGGLE_EDIT,
  SET_AUTH_STATE,
  GET_AUTH_STATE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function toggleEdit() {
  return {
    type: TOGGLE_EDIT,
  };
}
export function getAuthState() {
  return {
    type: GET_AUTH_STATE,
  };
}

export function setAuthState(payload) {
  return {
    type: SET_AUTH_STATE,
    payload,
  };
}

export function getProfileSuling(userId) {
  return {
    type: GET_PROFILE_SULING_REQUESTED,
    payload: {
      userId,
    },
  };
}

export function receiveProfileSulingSuccess(payload) {
  return {
    type: GET_PROFILE_SULING_SUCCESS,
    payload,
  };
}

export function receiveProfileSulingError(error) {
  return {
    type: GET_PROFILE_SULING_ERROR,
    payload: error,
  };
}

export function editProfileSuling(id, subtitle, location, phoneNo) {
  return {
    type: EDIT_PROFILE_SULING_REQUESTED,
    payload: {
      subtitle,
      location,
      phone_no: phoneNo,
      id,
    },
  };
}

export function receiveEditProfileSulingSuccess(payload) {
  return {
    type: EDIT_PROFILE_SULING_SUCCESS,
    payload,
  };
}

export function receiveEditProfileSulingError(error) {
  return {
    type: EDIT_PROFILE_SULING_ERROR,
    payload: error,
  };
}
