import json
from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from django.contrib.sessions.middleware import SessionMiddleware
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIRequestFactory
from datetime import date

from app_admin.models import UserRegistration, User
from app_admin.serializers import UserRegistrationSerializer


class AdminAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = 'johnsnow'
        self.name = 'John Snow'
        self.birthday = date(1990, 12, 11)
        self.active_email = 'john@snow.com'
        self.angkatan = 2012
        self.npm = '1234567890'
        self.pa_name = 'Ned Stark'
        self.user_regist = UserRegistration.objects.create(
            username=self.username,
            name=self.name,
            birthday=self.birthday,
            active_email=self.active_email,
            angkatan=self.angkatan,
            npm=self.npm,
            pa_name=self.pa_name,
        )

        self.username2 = 'johnsnow2'
        self.name2 = 'John Snow II'
        self.birthday2 = date(1990, 12, 11)
        self.active_email2 = 'john2@snow.com'
        self.angkatan2 = 2012
        self.npm2 = '1234567890'
        self.pa_name2 = 'Ned Stark'
        self.user_regist2 = UserRegistration.objects.create(
            username=self.username2,
            name=self.name2,
            birthday=self.birthday2,
            active_email=self.active_email2,
            angkatan=self.angkatan2,
            npm=self.npm2,
            pa_name=self.pa_name2,
        )

        self.user_admin = User.objects.create_superuser(
            username='admin',
            password='qwertyuiop',
            email='admin@email.com',
        )

        self.user_biasa = User.objects.create_user(
            username='biasa',
            password='qwertyuiop',
            email='bisa@email.com',
        )

    def test_admin_dashboard(self):
        url = '/api/admin/dashboard/'
        self.client.force_authenticate(self.user_admin)
        response = self.client.get(url)
        response_data = json.loads(response.content)

        self.assertEquals(200, response.status_code)
        self.assertAlmostEquals(2, response_data['mahasiswa_unverified_sum'])

    def test_admin_dashboard_without_admin_user(self):
        url = '/api/admin/dashboard/'
        self.client.force_authenticate(self.user_biasa)
        response = self.client.get(url)

        self.assertEquals(403, response.status_code)

    def test_admin_view_registration_list(self):
        url = '/api/admin/registration/mahasiswa/'
        self.client.force_authenticate(self.user_admin)
        response = self.client.get(url)
        response_data = json.loads(response.content)

        self.assertEquals(200, response.status_code)
        self.assertEquals(2, len(response_data))

    def test_admin_view_registration_list_without_admin_user(self):
        url = '/api/admin/registration/mahasiswa/'
        self.client.force_authenticate(self.user_biasa)
        response = self.client.get(url)

        self.assertEquals(403, response.status_code)

    def test_admin_view_registration_detail(self):
        url = '/api/admin/registration/mahasiswa/1/'
        self.client.force_authenticate(self.user_admin)
        response = self.client.get(url)
        response_data = json.loads(response.content)

        self.assertEquals(200, response.status_code)
        self.assertEquals(self.name, response_data['name'])

    def test_admin_view_registration_detail_no_user(self):
        url = '/api/admin/registration/mahasiswa/199/'
        self.client.force_authenticate(self.user_admin)
        response = self.client.get(url)

        self.assertEquals(404, response.status_code)

    def test_admin_verify_mahasiswa(self):
        url = '/api/admin/registration/mahasiswa/1/'
        self.client.force_authenticate(self.user_admin)
        response = self.client.post(url)
        response_data = json.loads(response.content)

        self.assertEquals(201, response.status_code)
        self.assertEquals('user verified', response_data['message'])

        created_user_arr = User.objects.filter(username=self.username)
        created_user = created_user_arr[0]
        self.assertEquals(self.username, created_user.username)

        user_regist = UserRegistration.objects.filter(id=1)
        self.assertAlmostEquals(0, len(user_regist))
