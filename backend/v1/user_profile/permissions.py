from rest_framework.permissions import BasePermission


class UserProfileIsOwner(BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user == obj.user_profile.user_mahasiswa.user


class UserProfileIsUser(BasePermission):

    def has_object_permission(self, request, view, obj):
        return request.user == obj.user_mahasiswa.user
