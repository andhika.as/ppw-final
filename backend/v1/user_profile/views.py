from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from user_profile.models import (
    UserProfile,
    Experience,
    Skill,
    UserCSAuthProfile,
    UserLinkedinProfile
)
from user_profile.permissions import UserProfileIsOwner, UserProfileIsUser
from user_profile.serializers import (
    UserLinkedinProfileSerializer,
    UserProfileSerializer,
    ExperienceSerializer,
    SkillSerializer,
    UserCSAuthProfileSerializer
)


class UserList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer


class UserDetail(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserProfileSerializer

    def get(self, request, pk, format=None):
        try:
            user_profile = UserProfile.objects.get(user_mahasiswa__id=pk)
            serializer = self.get_serializer(user_profile)
            return Response(serializer.data)
        except UserProfile.DoesNotExist:
            body = {
                'data': {
                    'message': 'profile not found',
                },
            }
            return Response(body, 404)


class UserDetailEdit(generics.RetrieveUpdateAPIView):
    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated, UserProfileIsUser)
    queryset = UserProfile.objects.all()

    def get(self, request, pk, format=None):
        try:
            user_profile = UserProfile.objects.get(user_mahasiswa__id=pk)
            req_id = self.request.user.user_mahasiswa.user_profile.id
            if int(pk) == int(req_id):
                serializer = self.get_serializer(user_profile)
                return Response(serializer.data)
            else:
                body = {
                    'data': {
                        'message': 'Akses Perboden',
                        },
                }
                return Response(body, 403)
        except UserProfile.DoesNotExist:
            body = {
                'data': {
                    'message': 'profile not found',
                },
            }
            return Response(body, 404)


class UserExperienceEdit(generics.RetrieveUpdateDestroyAPIView):
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer
    permission_classes = (permissions.IsAuthenticated, UserProfileIsOwner)

    def perform_update(self, serializer):
        serializer.save(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )


class UserSkillEdit(generics.RetrieveUpdateDestroyAPIView):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer
    permission_classes = (permissions.IsAuthenticated, UserProfileIsOwner)

    def perform_update(self, serializer):
        serializer.save(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )


class UserExperienceCreate(generics.ListCreateAPIView):
    serializer_class = ExperienceSerializer
    permission_classes = (permissions.IsAuthenticated, UserProfileIsOwner)

    def get_queryset(self):
        return Experience.objects.filter(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )

    def perform_create(self, serializer):
        serializer.save(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )


class UserSkillCreate(generics.ListCreateAPIView):
    serializer_class = SkillSerializer
    permission_classes = (permissions.IsAuthenticated, UserProfileIsOwner)

    def get_queryset(self):
        return Skill.objects.filter(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )

    def perform_create(self, serializer):
        serializer.save(
            user_profile=self.request.user.user_mahasiswa.user_profile
        )


class UserLinkedinProfileAPIView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserLinkedinProfileSerializer
    queryset = UserLinkedinProfile.objects.all()


class CSAuthDetails(generics.RetrieveAPIView):
    permissions_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = UserCSAuthProfile.objects.all()
    serializer_class = UserCSAuthProfileSerializer
