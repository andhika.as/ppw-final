import json

from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from django.contrib.sessions.middleware import SessionMiddleware
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework import (
    status
)

from user_profile.models import (
    UserProfile,
    Experience,
    Skill,
    UserLinkedinProfile
)
from user_profile.serializers import (
    UserProfileSerializer,
    ExperienceSerializer,
    SkillSerializer
)
from app_auth.models import User, UserMahasiswa
from app_auth.views import LoginMahasiswaAPIView
from datetime import date


class UserProfileListAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.role = "Mahasiswa"

        self.secondary_email = "john2@snow.com"
        self.phone_no = "080000"
        self.profpic_url = "http://www.img.com"

        self.exp_title = "lorem ipsum"
        self.exp_organization = "lorem"
        self.exp_description = "lorem ipsum dolor sit amet"
        self.exp_start_date = date(2018, 1, 1)
        self.exp_end_date = date(2018, 3, 1)
        self.exp_category = 'education'

        self.skill_title = 'jago'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        self.student = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        self.profile = UserProfile.objects.create(
            user_mahasiswa=self.student,
            secondary_email=self.secondary_email,
            phone_no=self.phone_no,
            profpic_url=self.profpic_url,
        )

        self.experience1 = Experience.objects.create(
            user_profile=self.profile,
            title=self.exp_title,
            organization=self.exp_organization,
            description=self.exp_description,
            start_date=self.exp_start_date,
            end_date=self.exp_end_date,
            category=self.exp_category
        )

        self.experience2 = Experience.objects.create(
            user_profile=self.profile,
            title=self.exp_title,
            organization=self.exp_organization,
            description=self.exp_description,
            start_date=self.exp_start_date,
            end_date=self.exp_end_date,
            category=self.exp_category
        )

        self.skill1 = Skill.objects.create(
            user_profile=self.profile,
            title=self.skill_title
        )

        self.skill2 = Skill.objects.create(
            user_profile=self.profile,
            title=self.skill_title
        )

        self.url = reverse("profile:detail", kwargs={"pk": self.profile.pk})
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()

    def test_authentication_with_invalid_data(self):
        """
        Test to verify auth in todo object bundle
        """
        response_login = self.client.post(
            '/auth/cs-auth/login/',
            {"username": "asdasd", "password": "asdasd"}
        )
        self.assertEqual(400, response_login.status_code)

        response = self.client.get('/api/profile/', {'user_mahasiswa': 1})
        expected = {'detail': 'Authentication credentials were not provided.'}
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(expected, response_data)

    def test_student_object_bundle(self):
        """
        Test to verify experience object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        student_serializer_data = UserProfileSerializer(
            instance=self.profile)
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(
            student_serializer_data.data['id'],
            response_data['id']
        )

    def test_view_without_login(self):
        expected = {'detail': 'Authentication credentials were not provided.'}
        response = self.client.get(self.url)
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(expected, response_data)

    def test_user_profile_not_found(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get('/api/profile/199/')
        expected_message = 'profile not found'

        self.assertEqual(404, response.status_code)
        self.assertEqual(expected_message, response.data['data']['message'])

    def test_get_user_profile_str(self):
        get_str = self.profile.__str__()
        expected = 'John Snow'
        self.assertEqual(expected, get_str)

    def test_get_experience_str(self):
        get_str = self.experience1.__str__()
        expected = 'lorem ipsum at lorem'
        self.assertEqual(expected, get_str)

    def test_get_skill_str(self):
        get_str = self.skill1.__str__()
        expected = 'jago'
        self.assertEqual(expected, get_str)


class UserProfileAddSkillExperienceAPITestCase(APITestCase):

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.role = "Mahasiswa"

        self.secondary_email = "john2@snow.com"
        self.phone_no = "080000"
        self.profpic_url = "http://www.img.com"

        self.exp_title = "lorem ipsum"
        self.exp_organization = "lorem"
        self.exp_description = "lorem ipsum dolor sit amet"
        self.exp_start_date = date(2018, 1, 1)
        self.exp_end_date = date(2018, 3, 1)
        self.exp_category = 'education'

        self.skill_title = 'jago'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        self.student = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        self.profile = UserProfile.objects.create(
            user_mahasiswa=self.student,
            secondary_email=self.secondary_email,
            phone_no=self.phone_no,
            profpic_url=self.profpic_url,
        )

        self.url_exp = '/api/profile/1/add/exp/'
        self.url_skill = '/api/profile/1/add/skill/'

    def test_create_exp(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url_exp,
            {
                "user_profile": 1,
                "title": "Asdos",
                "organization": "Fasilkom UI",
                "organization_logo_url": "http://image.google.com",
                "description": "Susah",
                "start_date": "2017-10-11",
                "end_date": "2018-4-1",
                "category": "JOB",
            }
        )

        self.assertEqual(201, response.status_code)

    def test_create_skill(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url_skill,
            {
                "user_profile": 1,
                "title": "Python",
            }
        )

        self.assertEqual(201, response.status_code)

    def test_create_exp_invalid_user(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url_exp,
            {
                "user_profile": 2,
                "title": "Asdos",
                "organization": "Fasilkom UI",
                "organization_logo_url": "http://image.google.com",
                "description": "Susah",
                "start_date": date(2017, 10, 11),
                "end_date": date(2018, 4, 1),
                "category": "job",
            }
        )

        self.assertEqual(400, response.status_code)

    def test_create_skill_invalid_user(self):
        self.client.force_authenticate(user=self.user)

        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url_skill,
            {
                "user_profile": 2,
                "title": "Python",
            }
        )

        self.assertEqual(400, response.status_code)


class UserProfileEditSkillExperienceAPITestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.role = "Mahasiswa"

        self.secondary_email = "john2@snow.com"
        self.phone_no = "080000"
        self.profpic_url = "http://www.img.com"

        self.exp_title = "lorem ipsum"
        self.exp_organization = "lorem"
        self.exp_description = "lorem ipsum dolor sit amet"
        self.exp_start_date = date(2018, 1, 1)
        self.exp_end_date = date(2018, 3, 1)
        self.exp_category = 'education'

        self.skill_title = 'jago'

        self.user = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
        )

        self.student = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        self.profile = UserProfile.objects.create(
            user_mahasiswa=self.student,
            secondary_email=self.secondary_email,
            phone_no=self.phone_no,
            profpic_url=self.profpic_url,
        )

        self.experience = Experience.objects.create(
            user_profile=self.profile,
            title=self.exp_title,
            organization=self.exp_organization,
            description=self.exp_description,
            start_date=self.exp_start_date,
            end_date=self.exp_end_date,
            category=self.exp_category
        )

        self.skill = Skill.objects.create(
            user_profile=self.profile,
            title=self.skill_title
        )

        self.url_exp = '/api/profile/1/edit/exp/1/'
        self.url_skill = '/api/profile/1/edit/skill/1/'

        self.username2 = "john2"
        self.email2 = "john@snow2.com"
        self.password2 = "you_know_nothing"
        self.role2 = "Mahasiswa"

        self.secondary_email2 = "john22@snow.com"
        self.phone_no2 = "080000"
        self.profpic_url2 = "http://www.img.com"

        self.exp_title2 = "lorem ipsum"
        self.exp_organization2 = "lorem"
        self.exp_description2 = "lorem ipsum dolor sit amet"
        self.exp_start_date2 = date(2018, 1, 1)
        self.exp_end_date2 = date(2018, 3, 1)
        self.exp_category2 = 'education'

        self.skill_title2 = 'jago'

        self.user2 = User.objects.create_user(
            username=self.username2,
            email=self.email2,
            password=self.password2,
        )

        self.student2 = UserMahasiswa.objects.create(
            user=self.user2,
            nama_lengkap="John Snow 2",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        self.profile2 = UserProfile.objects.create(
            user_mahasiswa=self.student2,
            secondary_email=self.secondary_email2,
            phone_no=self.phone_no2,
            profpic_url=self.profpic_url2,
        )

        self.experience2 = Experience.objects.create(
            user_profile=self.profile2,
            title=self.exp_title2,
            organization=self.exp_organization2,
            description=self.exp_description2,
            start_date=self.exp_start_date2,
            end_date=self.exp_end_date2,
            category=self.exp_category2
        )

        self.skill2 = Skill.objects.create(
            user_profile=self.profile2,
            title=self.skill_title2
        )

    def test_edit_exp(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_exp,
            {
                "user_profile": 1,
                "title": self.exp_title,
                "organization": self.exp_organization,
                "organization_logo_url": "http://image.google.com",
                "description": "ASIK",
                "start_date": self.exp_start_date,
                "end_date": self.exp_end_date,
                "category": self.exp_category.upper(),
            }
        )

        self.assertEqual(200, response.status_code)

    def test_edit_exp_user_not_exist(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_exp,
            {
                "user_profile": 3,
                "title": self.exp_title,
                "organization": self.exp_organization,
                "organization_logo_url": "http://image.google.com",
                "description": "ASIK",
                "start_date": self.exp_start_date,
                "end_date": self.exp_end_date,
                "category": self.exp_category.upper(),
            }
        )

        self.assertEqual(400, response.status_code)

    def test_edit_skill(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_skill,
            {
                "user_profile": 1,
                "title": "ayam"
            }
        )

        self.assertEqual(200, response.status_code)

    def test_edit_skill_user_not_exist(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_skill,
            {
                "user_profile": 3,
                "title": "ayam"
            }
        )

        self.assertEqual(400, response.status_code)

    def test_delete_exp_no_data(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete('/api/profile/1/edit/exp/3/')
        self.assertEqual(404, response.status_code)

    def test_delete_exp(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.delete(self.url_exp)
        self.assertEqual(204, response.status_code)

    def test_delete_skill_no_data(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete('/api/profile/1/edit/skill/3/')
        self.assertEqual(404, response.status_code)

    def test_delete_skill(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.delete(self.url_skill)
        self.assertEqual(204, response.status_code)

    def test_edit_other_user_skill(self):
        self.client.force_authenticate(user=self.user2)
        response = self.client.get(self.url_skill)
        self.assertEqual(403, response.status_code)

        response = self.client.put(
            self.url_skill,
            {
                "user_profile": 1,
                "title": "ayam"
            }
        )

        self.assertEqual(403, response.status_code)

    def test_edit_skill_with_another_user_id(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_skill)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_skill,
            {
                "user_profile": 2,
                "title": "ayam"
            }
        )

        response_data = json.loads(response.content)
        user_profile_id = response_data['user_profile']
        self.assertEqual(self.profile.id, user_profile_id)

    def test_edit_other_user_exp(self):
        self.client.force_authenticate(user=self.user2)
        response = self.client.get(self.url_exp)
        self.assertEqual(403, response.status_code)

        response = self.client.put(
            self.url_exp,
            {
                "user_profile": 1,
                "title": self.exp_title,
                "organization": self.exp_organization,
                "organization_logo_url": "http://image.google.com",
                "description": "ASIK",
                "start_date": self.exp_start_date,
                "end_date": self.exp_end_date,
                "category": self.exp_category.upper(),
            }
        )

        self.assertEqual(403, response.status_code)

    def test_edit_exp_with_another_user_id(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_exp)
        self.assertEqual(200, response.status_code)

        response = self.client.put(
            self.url_exp,
            {
                "user_profile": 2,
                "title": self.exp_title,
                "organization": self.exp_organization,
                "organization_logo_url": "http://image.google.com",
                "description": "ASIK",
                "start_date": self.exp_start_date,
                "end_date": self.exp_end_date,
                "category": self.exp_category.upper(),
            }
        )

        response_data = json.loads(response.content)
        user_profile_id = response_data['user_profile']
        self.assertEqual(self.profile.id, user_profile_id)

    def test_delete_other_user_skill(self):
        self.client.force_authenticate(user=self.user2)
        response = self.client.get(self.url_skill)
        self.assertEqual(403, response.status_code)

        response = self.client.delete(self.url_skill)
        self.assertEqual(403, response.status_code)

    def test_delete_other_user_exp(self):
        self.client.force_authenticate(user=self.user2)
        response = self.client.get(self.url_exp)
        self.assertEqual(403, response.status_code)

        response = self.client.delete(self.url_exp)
        self.assertEqual(403, response.status_code)


class ProfileLinkedinAPIViewTestCase(APITestCase):
    def setUp(self):
        self.first_name = "Affan"
        self.last_name = "Dhia"
        self.username = "affandhia"
        self.password = "affan4"
        self.headline = "So Much Wow"
        self.location = "Cilegon"
        self.industry = "SV .inc"
        self.current_share = 10
        self.num_connections = 600
        self.summary = "Yo boyo"
        self.specialties = "Getting girls"
        self.picture_url = "https://picsum.photos/200"
        self.public_profile_url = "https://picsum.photos/200"
        self.email_address = "affan@sv.com"
        self.user = User.objects.create_user(
            username=self.username,
            email=self.email_address,
            password=self.password,
        )
        self.user_linkedin = UserLinkedinProfile.objects.create(
            user=self.user,
            first_name=self.first_name,
            last_name=self.last_name,
            headline=self.headline,
            location=self.location,
            industry=self.industry,
            current_share=self.current_share,
            num_connections=self.num_connections,
            summary=self.summary,
            specialties=self.specialties,
            picture_url=self.picture_url,
            public_profile_url=self.public_profile_url,
            email_address=self.email_address,
        )
        self.factory = RequestFactory()

    def test_create_object_user_linkedin(self):
        temp_user = User.objects.create_user(
            username="affan1",
            email=self.email_address,
            password=self.password,
        )
        temp = UserLinkedinProfile.objects.create(
            user=temp_user,
            first_name="affan satu",
            last_name=self.last_name,
            headline=self.headline,
            location=self.location,
            industry=self.industry,
            current_share=self.current_share,
            num_connections=self.num_connections,
            summary=self.summary,
            specialties=self.specialties,
            picture_url=self.picture_url,
            public_profile_url=self.public_profile_url,
            email_address=self.email_address,
        )

        user = UserLinkedinProfile.objects.filter(
            first_name="affan satu")[:1].get()

        self.assertEqual(temp.first_name, user.first_name)

    def test_get_linkedin_profile_without_login(self):
        url_logout = reverse("profile:detail-linkedin",
                             kwargs={"pk": self.user.pk})
        response = self.client.get(url_logout)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_get_linkedin_profile_with_login(self):
        url_logout = reverse("profile:detail-linkedin",
                             kwargs={"pk": self.user.pk})
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(url_logout)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_linkedin_profile_invalid_pk(self):
        url_logout = reverse("profile:detail-linkedin", kwargs={"pk": 123123})
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(url_logout)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
