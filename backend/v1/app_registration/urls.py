from django.conf.urls import url
from app_registration.views import (
    SignUpAPIView,
    UserRegistrationListAPIView,
)


urlpatterns = [
    url(r'^$', SignUpAPIView.as_view(), name="signup"),
    url(r'^list/$', UserRegistrationListAPIView.as_view(), name="list"),
]
