from rest_framework.views import APIView
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status

from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
)
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser,
    AllowAny,
)
from app_registration.serializers import UserRegistrationSerializer
from app_registration.models import UserRegistration


class SignUpAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):

        try:
            __USERNAME__ = request.data['username']
            __EMAIL__ = request.data['email']
            __NAME__ = request.data['name']
            __BIRTHDAY__ = request.data['birthday']
            __ANGKATAN__ = request.data['angkatan']
            __NPM__ = request.data['npm']
            __PANAME__ = request.data['pa_name']

            user = User.objects.filter(Q(username=__USERNAME__) |
                                       Q(email=__EMAIL__))

            if len(user) > 0:

                message = {
                    "messages":
                    "user with that username or email already exist"
                }

                return Response(message, status=status.HTTP_400_BAD_REQUEST)
            else:
                user_registration = UserRegistration.objects.create(
                    username=__USERNAME__,
                    active_email=__EMAIL__,
                    name=__NAME__,
                    birthday=__BIRTHDAY__,
                    angkatan=__ANGKATAN__,
                    npm=__NPM__,
                    pa_name=__PANAME__,
                )

                message = {
                    "message": "user registration created"
                }

                return Response(message, status=status.HTTP_201_CREATED)

        except Exception as e:
            print(e)
            message = {
                "messages": "bad request"
            }
            return Response(message, status=status.HTTP_400_BAD_REQUEST)


class UserRegistrationListAPIView(ListAPIView):
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )
    serializer_class = UserRegistrationSerializer
    queryset = UserRegistration.objects.all()
