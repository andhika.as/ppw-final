from rest_framework import serializers
from django.contrib.auth.models import User
from app_auth.models import (
    UserMahasiswa,
    UserPerusahaan,
)
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'is_staff',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password', )


class UserMahasiswaSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserMahasiswa
        fields = ('id', 'nama_lengkap', 'npm', 'angkatan', 'user')


class UserPerusahaanSerializer(serializers.ModelSerializer):
    class Meta:
        models = UserPerusahaan
        fields = '_all_'
