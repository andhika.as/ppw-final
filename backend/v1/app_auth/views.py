import json
import requests
import os
import logging
import httplib2
import requests
from django.conf import settings
from django.contrib.auth import (
    login as auth_login,
    logout as auth_logout
)
from django.contrib.auth.models import User
from django.core.exceptions import (
    PermissionDenied, SuspiciousOperation
)
from django.http import (
    HttpRequest, HttpResponseRedirect,
    HttpResponse, JsonResponse,
    HttpResponseBadRequest
)
from django.http import (
    HttpResponseRedirect, HttpResponse
)
from django.core import serializers
from googleapiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from rest_framework import permissions
from rest_framework import status
from rest_framework.generics import (
    ListAPIView,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from django.conf import settings
from django.shortcuts import render
from django.http import (
    HttpRequest, HttpResponseRedirect, HttpResponse,
    JsonResponse
)
from django.contrib.auth import (
    login as auth_login,
    logout as auth_logout
)

from user_profile.models import UserProfile
from user_profile.serializers import (
    UserProfileSerializer,
    UserCSAuthProfileSerializer,
    UserLinkedinProfileSerializer,
)
from app_auth.utils import AuthHelper
from app_auth.models import UserMahasiswa, UserPerusahaan, User
from app_auth.serializers import (
    UserMahasiswaSerializer,
    UserPerusahaanSerializer,
    UserSerializer
)
from app_auth.utils import (
    AuthHelper,
    LinkedinHelper,
    JWTHelper
)
from app_auth.status_code import (
    HTTP_432_WRONG_PASSWORD_OR_USERNAME,
)
from rest_framework_jwt.settings import api_settings
from googleapiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from collections import defaultdict

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserMahasiswaAPIView(ListAPIView):
    queryset = UserMahasiswa.objects.all()
    serializer_class = UserMahasiswaSerializer
    permission_classes = (permissions.IsAuthenticated, )


class LogoutAPIView(APIView):
    def get(self, request, *args, **kwargs):
        response = Response()
        response.delete_cookie('jwt_')
        response.delete_cookie('user_id')
        response.delete_cookie('username')
        auth_logout(request)
        response.message = {
            'message': 'You has been successfully logged out.'
        }
        response.status = status.HTTP_200_OK
        return response


class LoginMahasiswaAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        try:
            __USERNAME__ = request.data['username']
            __PASSWORD__ = request.data['password']

            helper = AuthHelper()

            access_token = helper.get_access_token(__USERNAME__, __PASSWORD__)

            verified_user = helper.verify_user(access_token)

            if verified_user.status_code == status.HTTP_200_OK:
                verified_user = verified_user.json()

                if verified_user['role'] not in settings.VERIFIED_ROLE:
                    body = {
                        'data': {
                            'message': 'Permission Denied'
                        },
                    }
                    return Response(data=body,
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                body = {
                    'data': {
                        'message': 'Bad Request to API CS'
                    },
                }
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            user_mahasiswa_id = verified_user['identity_number']
            username = verified_user['username']
            role = verified_user['role']
            email = helper.get_email_by_username(username)
            user_mahasiswa = None
            try:
                user = User.objects.get(
                    email=email)

                user_mahasiswa = UserMahasiswa.objects.get(user_id=user.id)

            except User.DoesNotExist:

                user_csauth = helper.get_user_data(
                    access_token, user_mahasiswa_id)

                fullname = user_csauth['nama']
                firstname, lastname = helper.get_firstname_lastname(fullname)
                npm = user_csauth['npm']
                angkatan = helper.get_angkatan_by_npm(npm)

                user = User()
                user.username = username
                user.email = email
                user.first_name = firstname
                user.last_name = lastname
                user.save()

                user_mahasiswa = UserMahasiswa()
                user_mahasiswa.user = user
                user_mahasiswa.id = user.id
                user_mahasiswa.npm = npm
                user_mahasiswa.nama_lengkap = fullname
                user_mahasiswa.angkatan = angkatan
                user_mahasiswa.is_valid = True
                user_mahasiswa.save()

                user_profile = UserProfile()
                user_profile.user_mahasiswa = user_mahasiswa
                user_profile.id = user_mahasiswa.id
                user_profile.save()

                alamat = "{}, {}".format(
                    user_csauth["alamat_mhs"], user_csauth["kd_pos_mhs"]
                )
                kota_lahir = user_csauth["kota_lahir"]
                tanggal_lahir = user_csauth["tgl_lahir"]
                prodi, program = helper.get_prodi_and_program(user_csauth)
                cs_auth_profile = helper.save_user_csauth_profile(
                    user_mahasiswa, alamat, kota_lahir,
                    tanggal_lahir, prodi, program
                )

            serializer = UserMahasiswaSerializer(user_mahasiswa)

            # make user is authenticated
            auth_login(request, user)
            token = JWTHelper().get_jwt(user)
            body = {
                'data': {
                    'message': 'Login berhasil!',
                    'jwt': token,
                    'user_id': user.id,
                    'user': serializer.data,
                }
            }
            return Response(data=body, status=status.HTTP_200_OK)

        except Exception as e:
            message = {
                'message': 'Bad Request'
            }
            return Response(message, status=status.HTTP_401_UNAUTHORIZED)


class LoginPerusahaanAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def authenticate(self, username, password):

        try:
            user = User.objects.get(username=username)

            if user.password == password:
                return user
            else:
                message = {
                    'message': 'Wrong Password'
                }
                return Response(message, status=status.HTTP_401_UNAUTHORIZED)

        except User.DoesNotExist:
            message = {
                'message': 'Username Not Found'
            }
            return Response(message, status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request, format=None):
        __USERNAME__ = request.data['username']
        __PASSWORD__ = request.data['password']

        user = self.authenticate(__USERNAME__, __PASSWORD__)

        user_perusahaan = UserPerusahaan.objects.get(user=user.id)

        serializer = UserPerusahaanSerializer(user_perusahaan)

        auth_login(request, user_perusahaan)

        return Response(serializer.data, status=status.HTTP_200_OK)


class LogoutAPIView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            response = Response()
            response.delete_cookie('jwt_')
            response.delete_cookie('user_id')
            response.delete_cookie('username')
        except Exception:
            pass

        auth_logout(request)

        message = {
            'message': 'Successfully logged out'
        }

        response.message = message
        response.status = status.HTTP_200_OK
        return response


class LinkedinRequestAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        CLIENT_ID_LINKEDIN = os.getenv('CLIENT_ID_LINKEDIN', '')
        request.session['state'] = LinkedinHelper().get_random_state()
        BE_URL = os.getenv('BE_URL', '')
        REDIRECT_URI = BE_URL + "auth/linkedin/callback"
        uri = (
            "https://www.linkedin.com/oauth/v2/authorization?" +
            "response_type=code&" +
            "client_id=" + CLIENT_ID_LINKEDIN + "&" +
            "redirect_uri=" + REDIRECT_URI + "&" +
            "state=" + request.session['state'] + "&" +
            "scope=r_basicprofile r_emailaddress rw_company_admin w_share"
        )
        return HttpResponseRedirect(uri)


class LinkedinCallbackAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        BE_URL = os.getenv('BE_URL', '')
        CLIENT_ID_LINKEDIN = os.getenv('CLIENT_ID_LINKEDIN', '')
        CLIENT_SECRET_LINKEDIN = os.getenv(
            'CLIENT_SECRET_LINKEDIN', ''
        )
        REDIRECT_URI = BE_URL + "auth/linkedin/callback"
        request_token_uri = "https://www.linkedin.com/oauth/v2/accessToken"

        error = request.GET.get('error', '')
        if error != '':
            return HttpResponse(status=401)

        state = request.GET.get('state', '')
        if state != request.session['state']:
            return HttpResponse(status=403)

        code = request.GET.get('code', '')
        request_data = {
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": REDIRECT_URI,
            "client_id": CLIENT_ID_LINKEDIN,
            "client_secret": CLIENT_SECRET_LINKEDIN
        }

        request_header = {
            'content_type': "application/json"
        }

        response = requests.request(
            "POST", request_token_uri,
            data=request_data,
            headers=request_header)
        response_data = json.loads(response.content.decode('utf8'))

        token = response_data['access_token']
        scope = (
            "first-name,last-name,headline,location,industry,"
            "current-share,num-connections,summary,specialties,"
            "positions,picture-url,public-profile-url,email-address"
        )
        url = (
            "https://api.linkedin.com/v1/people/~:({})?".format(scope) +
            "oauth2_access_token={}&format=json".format(token)
        )

        user_data = defaultdict(lambda: None)
        user_data_tmp = requests.request("GET", url=url)
        user_data_tmp = json.loads(user_data_tmp.content.decode('utf8'))
        user_data.update(user_data_tmp)
        email_address = user_data["emailAddress"]
        first_name = user_data["firstName"]
        last_name = user_data["lastName"]
        headline = user_data["headline"]
        industry = user_data["industry"]
        location = user_data["location"]["name"]
        number_connections = user_data["numConnections"]
        current_share = user_data["currentShare"]
        summary = user_data["summary"]
        picture_url = user_data["pictureUrl"]
        public_profile_url = user_data["publicProfileUrl"]
        specialties = user_data["specialties"]
        fullname = "{}{}".format(first_name, last_name)

        try:
            user = User.objects.get(email=email_address)
        except User.DoesNotExist:
            user = AuthHelper().save_user_profile(
                email_address,
                first_name,
                last_name
            )
            user_mahasiswa = AuthHelper().save_user_mahasiswa_profile(
                user, None, fullname, None
            )
            AuthHelper().save_user_linkedin_profile(
                user, first_name, last_name, headline, location, industry,
                current_share, number_connections, summary,
                specialties, picture_url, public_profile_url, email_address
            )
            user_profile = UserProfile()
            user_profile.user_mahasiswa = user_mahasiswa
            user_profile.id = user_mahasiswa.id
            user_profile.save()
        # make user is authenticated
        auth_login(request, user)
        redirect_uri = os.getenv('FE_URL_V2', '') + "mahasiswa"
        response = HttpResponseRedirect(redirect_uri)
        response.set_cookie("user_id", user.id)
        response.set_cookie("username", user.username)
        response = JWTHelper().store_to_cookies(response, user, 3)
        return response


class GoogleAuthBase():
    def get_flow(self):
        redirect_uri = os.getenv('BE_URL', '') + 'auth/google/callback'
        FLOW = flow_from_clientsecrets(
            settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON,
            scope=[
                'https://www.googleapis.com/auth/plus.login',
                'https://www.googleapis.com/auth/plus.profile.emails.read',
                'https://www.googleapis.com/auth/userinfo.profile'
            ],
            redirect_uri=redirect_uri)
        return FLOW


class GoogleRequestAuthAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        FLOW = GoogleAuthBase().get_flow()
        authorize_url = FLOW.step1_get_authorize_url()
        return HttpResponseRedirect(authorize_url)


class GoogleCallbackAuthAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):
        if "error" in request.get_full_path():
            return HttpResponseBadRequest()
        FLOW = GoogleAuthBase().get_flow()
        credential = FLOW.step2_exchange(request.GET)

        http = httplib2.Http()
        http = credential.authorize(http)

        service = build("plus", "v1", http=http)
        people_resource = service.people()
        people_document = people_resource.get(userId='me').execute()

        email = people_document['emails'][0]['value']
        firstname = people_document['name']['givenName']
        lastname = people_document['name']['familyName']
        fullname = "{} {}".format(firstname, lastname)

        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            user = AuthHelper().save_user_profile(email,
                                                  firstname, lastname)
            AuthHelper().save_user_mahasiswa_profile(user,
                                                     None, fullname, None)

        # make user is authenticated
        auth_login(request, user)
        redirect_uri = os.getenv('FE_URL_V1', '') + "mahasiswa"
        response = HttpResponseRedirect(redirect_uri)
        response.set_cookie("user_id", user.id)
        response.set_cookie("username", user.username)
        response = JWTHelper().store_to_cookies(response, user, 3)
        return response


class LoginAdminApiView(APIView):
    permission_classes = (permissions.AllowAny,)

    def authenticate(self, username, password):
        try:
            user = User.objects.get(username=username)

            if user.check_password(password):
                return user
            else:
                message = {
                    'message': 'Wrong Password'
                }
                return Response(message, status=status.HTTP_401_UNAUTHORIZED)

        except User.DoesNotExist:
            message = {
                'message': 'Username Not Found'
            }
            return Response(message, status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request, format=None):
        __USERNAME__ = request.data['username']
        __PASSWORD__ = request.data['password']

        res = self.authenticate(__USERNAME__, __PASSWORD__)

        if type(res) == Response:
            return res

        user = res
        if not user.is_staff:
            message = {
                'message': 'Permission denied'
            }
            return Response(message, status=status.HTTP_403_FORBIDDEN)

        serializer = UserSerializer(user)

        auth_login(request, user)
        token = JWTHelper().get_jwt(user)
        payload = {
            'data': {
                'message': 'Login berhasil!',
                'jwt': token,
                'user': serializer.data,
            }
        }

        response = Response(data=payload, status=status.HTTP_200_OK)

        return response


class LoginUserApiView(APIView):
    permission_classes = (permissions.AllowAny, )

    def post(self, request):

        try:
            __USERNAME__ = request.data['username']
            __PASSWORD__ = request.data['password']

        except KeyError:
            body = {
                'message': 'Silahkan masukan username dan pasword anda!',
            }
            return Response(data=body, status=status.HTTP_401_UNAUTHORIZED)

        try:
            user = User.objects.get(
                username=__USERNAME__, password=__PASSWORD__)
            auth_login(request, user)
            user_mahasiswa = UserMahasiswa.objects.get(user=user)
            serializer = UserMahasiswaSerializer(user_mahasiswa)

            token = JWTHelper().get_jwt(user)
            payload = {
                'data': {
                    'message': 'Login berhasil!',
                    'jwt': token,
                    'user': serializer.data,
                }
            }
            return Response(data=payload, status=status.HTTP_200_OK)

        except User.DoesNotExist:
            body = {
                'message': 'Username atau password anda salah!',
            }
            return Response(
                data=body,
                status=HTTP_432_WRONG_PASSWORD_OR_USERNAME
            )


class VerifyAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        is_staff = request.user.is_staff
        user_serialized = None

        if not is_staff:
            serializer = UserProfileSerializer(
                request.user.user_mahasiswa.user_profile
            )
            user_serialized = serializer.data

        token = JWTHelper().get_jwt(request.user)
        body = {
            'data': {
                'message': 'You have been authorized',
                'jwt': token,
                'user_id': request.user.id,
                'user': user_serialized,
                'admin': str(is_staff),
            }
        }
        return Response(body, 200)
